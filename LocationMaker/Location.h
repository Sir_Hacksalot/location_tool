#pragma once
#include <boost\serialization\access.hpp>
#include <boost\serialization\vector.hpp>
#include <boost\serialization\map.hpp>
#include <boost\serialization\string.hpp>

#include <string>
#include <vector>
#include <map>

class Location
{
public:
	Location();
	~Location();

	void getInformation();

	std::string description;
	int locationNumber;

	std::map<std::string, int> exits;

	//item that we can pick up
	std::string item;

private:
	std::string getDescription();
	std::string getAvailableExits();

	friend class boost::serialization::access;
	friend std::ostream &operator<<(std::ostream &os, const Location &loc);

	template<class Archive>
	void serialize(Archive &ar, const unsigned int)
	{
		ar &description;
		ar &locationNumber;
		ar &item;
		ar &exits;
	}

};

