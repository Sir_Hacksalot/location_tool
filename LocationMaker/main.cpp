#include <iostream>
#include "Location.h"
#include "FileManager.h"
#include <vector>
#include <string>

int main()
{
	//create a vector to hold our locations
	std::vector<Location> locations;

	//ask the user how many locations there are going to be
	int totalLocations;
	std::cout << "How many locations are there? ";
	std::cin >> totalLocations;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(),
		'\n');

	for (int i = 0; i < totalLocations; i++)
	{
		std::cout << "LOCATION " << i + 1 << std::endl;
		std::cout << "----------\n";
		//create an object
		Location loc;
		//get the number
		loc.locationNumber = i + 1;
		//get the description
		std::cout << "Enter the location description: ";
		std::getline(std::cin, loc.description);
		//get the exits - north, east, south, west
		std::cout << "Enter the exit for North: ";
		int dir;
		std::cin >> dir;
		loc.exits.emplace("north", dir);
		std::cout << "Enter the exit for East: ";
		std::cin >> dir;
		loc.exits.emplace("east", dir);
		std::cout << "Enter the exit for South: ";
		std::cin >> dir;
		loc.exits.emplace("south", dir);
		std::cout << "Enter the exit for West: ";
		std::cin >> dir;
		loc.exits.emplace("west", dir);
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(),
			'\n');
		std::cout << "What item does the room contain (enter 'none' if no item): ";
		std::getline(std::cin, loc.item);
		//add to the vector
		locations.push_back(loc);
	}

	//save the file
	FileManager::save(locations, "locations.dat");

	//create a new vector to restore our data to
	std::vector<Location> newVector;
	//restore the data
	FileManager::restore(newVector, "locations.dat");

	//check the contents of the object
	for (Location loc : newVector)
	{
		std::cout << "Description: " << loc.description << std::endl;
	}

	system("PAUSE");
	return 0;
}