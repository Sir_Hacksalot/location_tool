#include "Location.h"
#include <iostream>



Location::Location()
{
}


Location::~Location()
{
}

void Location::getInformation()
{
	std::cout << getDescription() << std::endl << std::endl;
	std::cout << getAvailableExits() << std::endl << std::endl;
}

std::string Location::getDescription()
{
	return description;
}

std::string Location::getAvailableExits()
{
	std::string output = "You may exit to the ";

	for (auto iter = exits.begin(); iter != exits.end(); iter++)
	{
		//find the ones that are available - unavailable exits are 0
		if (iter->second != 0)
		{
			output += iter->first + " ";
		}
	}

	return output;
}

std::ostream & operator<<(std::ostream & os, const Location & loc)
{
	return os << loc.description << loc.locationNumber << loc.item;
}
